Movement
========
##### A simple game that was definitely *not* rushed!  
###### Made by Albert Zhang, Ethan Moyer, Ben Zydney, Winston Reese, Oliver Paulson and Bryant Stangel using the MIT licensed [Phaser](http://phaser.io) game engine.
Our gamse was created by several beginner coders in a language none of us were
proficient in due to TSA rules. We tried us to make this game extremely simple 
because we're a bunch of lazy procrastinators.

[]: # (if the line is too long just make nl w/o 2 space)
![Nothing here](http://puu.sh/muERl/5934ffbd8e.png)

This game was inspired by those flash game websites that have a million small games that are kind of fun for like 10 minutes,
and basically have some kind of small trick every level.

