/*global Phaser*/
// Define Vars
var player, cursors, velocity, ledge, walls, pause, end, start, spikes, spike, spaceKey, button, enterKey, pressEnter, win, menuState, gameOver, ending, x;
var velocity = -300;
var ledge_color = 0x555555;
var pause_scale = 0;
var spike_color = 0xFF6347;
var key_color = 0xffff00;
var can_skip = false;
var x = false;
setTimeout(function(){ can_skip = true; }, 180500);
function something() {
    if (!x) {
        game.state.start("level1");
    }
}
// Our Pause Function
function togglePause() {
    game.physics.arcade.isPaused = (game.physics.arcade.isPaused) ? false : true;
    pause_scale += 1;
    pause.scale.setTo((pause_scale % 2) * .9);
    win.visible = true;
}

function startGame() {
        menuState += 1;

    }
// Our Update Function, shared between (most) all levels
function update() {
    game.physics.arcade.collide(walls, player);
    if (cursors.left.isDown && cursors.right.isDown && can_skip) {
         ending.visible = true;
    }
    if ((cursors.up.isDown || game.input.activePointer.isDown) && player.body.touching.down) {
        player.body.velocity.y = -550;
    }
    if (player.body.touching.right || player.body.touching.left) {
        if ((cursors.up.isDown || game.input.activePointer.isDown) && !player.body.touching.down) {
            player.body.velocity.y = -520;
            ready = false;
        }
        velocity *= -1;
        player.body.velocity.x = velocity;
    }
}

// Function to init player
function add_player(x, y) {
    player = game.add.sprite(x, y, 'block');

    player.tint = 0x0099CC;
    game.physics.arcade.enable(player);
    player.scale.setTo(1, 2);
    player.body.gravity.y = 1000;
    player.body.velocity.x = velocity;
    player.anchor.setTo(.5);
}

// Function to set up level
function setup_level() {
    velocity = -300;
    //Start the game
    game.physics.startSystem(Phaser.Physics.ARCADE);
    //load in background
    var sky = game.add.graphics(0, 0);
    sky.beginFill(0xCCCCCC, 1);
    sky.drawRect(0, 0, game.world.width, game.world.height);
    walls = game.add.group();
    ledge = game.add.sprite(0, 608, 'block');
    ledge.tint = ledge_color;
    ledge.scale.setTo(20, 1);
    game.physics.arcade.enable(ledge);
    ledge.enableBody = true;
    ledge.body.immovable = true;
    walls.add(ledge);
    ledge = game.add.sprite(608, 0, 'block');
    ledge.tint = ledge_color;
    ledge.scale.setTo(1, 20);
    game.physics.arcade.enable(ledge);
    ledge.enableBody = true;
    ledge.body.immovable = true;
    walls.add(ledge);
    ledge = game.add.sprite(0, 0, 'block');
    ledge.tint = ledge_color;
    ledge.scale.setTo(1, 20);
    game.physics.arcade.enable(ledge);
    ledge.enableBody = true;
    ledge.body.immovable = true;
    walls.add(ledge);
    ledge = game.add.sprite(0, 0, 'block');
    ledge.tint = ledge_color;
    ledge.scale.setTo(20, 1);
    game.physics.arcade.enable(ledge);
    ledge.enableBody = true;
    ledge.body.immovable = true;
    walls.add(ledge);
    spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    spaceKey.onDown.add(togglePause, this);
    spikes = game.add.group();
    game.physics.arcade.enable(spikes);
    enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    
}

var level0 = {
    preload: function() {
        //   game.load.image('ground', 'assets/stuff.png');
        game.load.image('block', 'assets/tile.png');
        game.load.image('pause', 'assets/pause.png');
        game.load.image('menu', 'assets/Title1.png');
        game.load.image('PressEnter', 'assets/PressEnter.png');
        game.load.image('instruction', 'assets/instruction.png');
        game.load.image('intro', 'assets/intro.png');
        //   game.load.image('wall', 'assets/Walls.png');
    },

    create: function() {
        setup_level();

        button = game.add.sprite(game.world.centerX - 170, game.world.centerY - 32, 'menu');
        button.scale.setTo(1.5, 1.5);

        pause = game.add.sprite(game.world.centerX, game.world.centerY, 'pause');
        pause.anchor.setTo(.5);
        pause.scale.setTo(0);

        pressEnter = game.add.sprite(game.world.centerX - 128, game.world.centerY + 32, 'PressEnter');

        //make input keys
        cursors = game.input.keyboard.createCursorKeys();
        game.input.mouse.capture = true;

        var instruction = game.add.sprite(0, 0, 'instruction');
        instruction.visible = false;
        var intro = game.add.sprite(0, 0, 'intro');
        intro.visible = false;
        },
    update: function() {
    if ((enterKey.isDown || game.input.activePointer.isDown) && menuState == null )
    {
        var instruction = game.add.sprite(0, 0, 'instruction');
        instruction.visible = true;
        menuState = 1;
    }
    if ((enterKey.isDown || game.input.activePointer.isDown) && menuState == 1 )
    {
        var intro = game.add.sprite(0, 0, 'intro');
        intro.visible = true;
        setTimeout(something, 12000);
    }
    if ((cursors.right.isDown || game.input.activePointer.isDown) && menuState == 1 )
    {
        game.state.start('level1');
    }
}
};

var level1 = {
    preload: function() {
        //   game.load.image('ground', 'assets/stuff.png');
        game.load.image('block', 'assets/tile.png');
        game.load.image('pause', 'assets/pause.png');
        game.load.image('rightArrow', 'assets/right.png');
        //   game.load.image('wall', 'assets/Walls.png');
    },

    create: function() {
        x = true;
        setup_level();
        ledge = game.add.sprite(320, 32 * 13, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(10, 2);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);
        ledge = game.add.sprite(0, 32 * 6, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(10, 2);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);

        end = game.add.sprite(32,32,'block');
        end.scale.setTo(1, 5);
        end.tint = 0x90EE90;

        game.physics.arcade.enable(end);
        var rightArrow = game.add.sprite(192, 128, 'rightArrow');
        rightArrow.angle = -180;

        var upArrow = game.add.sprite(game.world.centerX - 214, game.world.centerY + 182, 'rightArrow');
        upArrow.angle = -90;
        //create player
        add_player(640-64, 640-64);
        game.add.tween(ledge).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        pause = game.add.sprite(game.world.centerX, game.world.centerY, 'pause');
        pause.anchor.setTo(.5);
        pause.scale.setTo(0);

        //make input keys
        cursors = game.input.keyboard.createCursorKeys();
    },
    update: function() {
        if (game.physics.arcade.overlap(player, end)) {
            game.state.start('level2')
        }
        update();
    }

};


var level2 = {
    preload: function() {
        //   game.load.image('ground', 'assets/stuff.png');
        game.load.image('block', 'assets/tile.png');
        game.load.image('pause', 'assets/pause.png');
        //   game.load.image('wall', 'assets/Walls.png');
    },

    create: function() {
        setup_level();
        game.physics.arcade.enable(end);
        //create player
        add_player(48, 640-64);

        pause = game.add.sprite(game.world.centerX, game.world.centerY, 'pause');
        pause.anchor.setTo(.5);
        pause.scale.setTo(0);
        //place left platform
        ledge = game.add.sprite(0, 32 * 13, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(9.5, 1);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);
        //place middle platform
        ledge = game.add.sprite(640-320, 32 * 7, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(10, 1);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);
        //create bottom spike
        spike = game.add.sprite(640-256, 640-32, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(3, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike)
        //lol
        spike = game.add.sprite(160, 640-32, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(3, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike)
        
        // spike = game.add.sprite(608, 32 * 14, 'block');
        // spike.tint = spike_color;
        // spike.scale.setTo(1, 1);
        // game.physics.arcade.enable(spike);
        // spike.enableBody = true;
        // spike.body.immovable = true;
        // spikes.add(spike)
        //create top spike
        spike = game.add.sprite(64, 32 * 12, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(4, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike)
        //Ending
        end = game.add.sprite(640-64,32,'block');
        end.scale.setTo(1, 6);
        end.tint = 0x90EE90;
        game.physics.arcade.enable(end);
    },
    update: function() {
        if (game.physics.arcade.overlap(player, spikes)) {
            game.state.start('level2');
        }
        update();
        if (game.physics.arcade.overlap(player, end)) {
            game.state.start("level3");
        }
    }

};

var level3 = {
    preload: function() {
        //   game.load.image('ground', 'assets/stuff.png');
        game.load.image('win', 'assets/Win.png');
        game.load.image('block', 'assets/tile.png');
        game.load.image('pause', 'assets/pause.png');
        game.load.image('ending', 'assets/ending.png');
        //   game.load.image('wall', 'assets/Walls.png');
    },

    create: function() {
        setup_level();
        ledge = game.add.sprite(game.world.centerX - 64, 32 * 15, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(5, 1);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);

        ledge = game.add.sprite(game.world.centerX + 64, 32 * 11, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(7, 1);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);

        ledge = game.add.sprite(32, 32 * 9, 'block');
        ledge.tint = ledge_color;
        ledge.scale.setTo(7, 1);
        game.physics.arcade.enable(ledge);
        ledge.enableBody = true;
        ledge.body.immovable = true;
        walls.add(ledge);

        spike = game.add.sprite(game.world.centerX, 32 * 14, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(2, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike);

        spike = game.add.sprite(640 - 192, 32 * 10, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(2, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike);

        spike = game.add.sprite(32 * 2, 32 * 9, 'block');
        spike.tint = spike_color;
        spike.scale.setTo(4, 1);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.immovable = true;
        spikes.add(spike);

        end = game.add.sprite(32,32,'block');
        end.scale.setTo(1, 8);
        end.tint = 0x90EE90;
        game.physics.arcade.enable(end);
        //create player
        add_player(640-64, 640-64);

        pause = game.add.sprite(game.world.centerX, game.world.centerY, 'pause');
        pause.anchor.setTo(.5);
        pause.scale.setTo(0);


        //make input keys
        cursors = game.input.keyboard.createCursorKeys();
    },
    update: function() {
        if (game.physics.arcade.overlap(player, spikes) && gameOver == null) {
            game.state.start('level3');
        }
        if (game.physics.arcade.overlap(player, end)) {
            player.visible = false;
            ending = game.add.sprite(0, 0, 'ending');
            player.body.immovable = true;
            ending.visible = true;
            gameOver = 1;
        }
        if (cursors.left.isDown && cursors.right.isDown && can_skip) {
             ending = game.add.sprite(0, 0, 'ending');
             ending.visible = true;
             gameOver = 1;
             player.visible = false;
        }
        update();
    }

};
//sets up our game
var game = new Phaser.Game(640, 640, Phaser.AUTO, '');
game.state.add('level0', level0);
game.state.add('level1', level1);
game.state.add('level2', level2);
game.state.add('level3', level3);
game.state.start('level0');
